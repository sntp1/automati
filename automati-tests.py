from automati import NFA, DFA
import unittest


class DoubleTest(unittest.TestCase):
    tests = [
        ('+123.345e123', True),
        ('123.456E+123', True),
        ('.123e-1', True),
        ('-1', True),
        ('-.1', True),
        ('.1', True),
        ('1.', True),
        ('+1.', True),
        ('.', False),
        ('+.', False),
        ('1', True),
        ('+1', True),
        ('-.1', True),
        ('dfasfa', False),
        ('+123.e3', False),
        ('+-1.2e2', False),
        ('123..e123', False),
        ('.e1', False),
        ('+', False),
        ('e123', False),
        ('123.34E-+1', False),
        ('-.e10', False)
    ]

    def setUp(self):
        self.dfa = DFA('resources/double.json')

    def test_test(self):
        for test in self.tests:
            self.dfa.reset_state()
            self.dfa.raw_input(test[0])
            self.assertEqual(self.dfa.is_in_accept_state(), test[1])


class ZeroTest(unittest.TestCase):
    tests = [
        ('1010', True),
        ('101', False),
        ('', False),
        ('1', False),
        ('0', True),
        ('00101010wd', False)
    ]

    def setUp(self):
        self.nfa = NFA('resources/0.json')

    def test_test(self):
        for test in self.tests:
            self.nfa.reset_state()
            self.nfa.raw_input(test[0])
            self.assertEqual(self.nfa.is_in_accept_state(), test[1])


if __name__ == '__main__':
    unittest.main()