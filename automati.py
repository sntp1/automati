import json
from collections import defaultdict


class DFA(object):
    def __init__(self, states, alphabet, transitions, start_state, accept_states):
        self.states = states
        self.alphabet = alphabet
        self.transitions = transitions
        self.start_state = start_state
        self.current_state = start_state
        self.accept_states = accept_states

    def __init__(self, filename):
        with open(filename) as f:
            automaton = json.load(f)
            self.states = automaton['states']
            self.alphabet = automaton['alphabet']
            self.start_state = automaton['start_state']
            self.current_state = automaton['start_state']
            self.accept_states = automaton['accept_states']
            self.transitions = {}
            for transition_function in automaton['transitions']:
                for input_value in transition_function['input']:
                    self.transitions[transition_function['state'], input_value] = transition_function['transition']

    def input(self, value):
        if (self.current_state, value) not in self.transitions.keys():
            self.current_state = None
        else:
            self.current_state = self.transitions[self.current_state, value]

    def raw_input(self, value):
        for c in value:
            self.input(c)

    def is_in_accept_state(self):
        return self.current_state in self.accept_states

    def reset_state(self):
        self.current_state = self.start_state

    def is_stopped(self):
        return self.current_state is None


class NFA(object):
    current_states = None

    def __init__(self, states, alphabet, transitions, start_state, accept_states):
        self.states = states
        self.alphabet = alphabet
        self.transitions = transitions
        self.start_state = start_state
        self.reset_state()
        self.accept_states = accept_states

    def __init__(self, filename):
        with open(filename) as f:
            automaton = json.load(f)
            self.states = automaton['states']
            self.alphabet = automaton['alphabet']
            self.start_state = automaton['start_state']
            self.reset_state()
            self.accept_states = automaton['accept_states']
            self.transitions = defaultdict(set)
            for transition_function in automaton['transitions']:
                for input_value in transition_function['input']:
                    self.transitions[transition_function['state'], input_value].add(transition_function['transition'])

    def input(self, value):
        updated = False
        new_states = set()
        for state in self.current_states:
            if (state, value) in self.transitions:
                for transition in self.transitions[state, value]:
                    new_states.add(transition)
                updated = True
        if not updated:
            self.current_states.clear()
        else:
            self.current_states = new_states

    def raw_input(self, value):
        for c in value:
            self.input(c)

    def is_in_accept_state(self):
        return len(self.current_states.intersection(self.accept_states)) > 0

    def reset_state(self):
        self.current_states = set([self.start_state])

    def is_stopped(self):
        return len(self.current_states) == 0
