from automati import NFA, DFA


def task(a, s, n):
    m = 0
    recognized = False
    for c in s[n:]:
        a.input(c)
        if a.is_stopped():
            break
        else:
            recognized = True
        m += 1
    return recognized, m


if __name__ == '__main__':
    dfa = DFA('resources/double.json')
    print task(dfa, 'sfhj123q', 4)
